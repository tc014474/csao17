#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void){
	int pfds[2], pfds2[2];
	pipe(pfds);
	pipe(pfds2);

	if (!fork()) {
		//child process for ls
		close (1); /* close normal stdout i.e. monitor */
		dup(pfds[1]);
		close(pfds[0]);
		close(pfds2[0]);
		close(pfds2[1]);
		execlp("ls", "ls", NULL);
	} else if (!fork()) {
		//child process for cut
		close(0);
		close(1);
		dup(pfds[0]);
		dup(pfds2[1]);
		close(pfds[1]);
		close(pfds2[0]);
		execlp("cut", "cut", "-b", "1-3", NULL);
	} else {
		//parent process for sort
		close(0);
		dup(pfds2[0]);
		close(pfds[0]);
		close(pfds[1]);
		close(pfds2[1]);
		execlp("sort", "sort", NULL);
	}
	return 0;
}

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<pthread.h>
#define N 3 //define the total number of threads we want

float total = 0; // set global variable

void* compute(void* arg) { //compute function just does something.
	int pid = *(int*)arg;
	int i;
	float result = 0;
	for(i = 0; i < 2000000000; i++) { //for a large number of times
		result = sqrt(1000.0) * sqrt(1000.0);
	}
	printf("Result of %d is %f\n", pid, result); //Prints the result
	total += result; //to keep a running total in the global variable total
	printf("Total of %d is %f\n", pid, total);
	return NULL;
}

int main() {
	pthread_t tid[N];
	int i, pid[N];

	for(i=0; i<N; i++) { //to loop and create the required number of threads
		pid[i] = i;
		if(pthread_create(&tid[i], NULL, &compute, &pid[i]) != 0) { //Do the thread creation and catch it if it/one fails
			perror("pthread_create");
			exit(1);
		} else { //give a message about the thread ID
			printf("Thread id for thread %d is %ld\n", i, tid[i]);
		}
	}
	//Wait for all threads to finish
	for(i=0; i<N; i++) {
		pthread_join(tid[i], NULL);
	}
	return 0;
}

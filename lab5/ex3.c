#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#define rowsize 5
#define colsize 5

int matrix[rowsize][colsize];

void *sum(void *arg) {
	printf("\n\nHello from thread %u\t \n", (unsigned int)pthread_self());

	int *row_element = (int *)arg;
	int sum = 0, i = 0;

	for(i = 0; i < colsize; i++) {
		printf("i = %d, Value at this position = %d\n", i, *(row_element+i));
		sum += *(row_element+i);
	}

	printf("\nSum is %d\n", sum);
	pthread_exit(NULL);
}

int main() {
	int i = 0, j;

	printf("Enter the elements of the matrix \n\n");
	for(i = 0; i < rowsize; i++)
		for(j = 0; j < colsize; j++)
			scanf("%d", &matrix[i][j]);

	printf("\nCreated Matrix\n\n");
	for(i=0; i<rowsize; i++) {
		for(j=0; j<colsize; j++)
			printf("%d\t", matrix[i][j]);
		printf("\n");
	}

	pthread_t thread_id[rowsize];
	for(i=0; i<rowsize; i++) {
		int t = pthread_create(&thread_id[i], NULL, sum, (void *)(matrix[i]));
		if(t) {
			printf("Unable to create thread\n");
			exit(0);
		}
	}

	pthread_exit(NULL);
}

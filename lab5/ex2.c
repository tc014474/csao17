#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdint.h>
#define MAX_THREADS 10

pthread_t thread_id[MAX_THREADS];

void * PrintHello(void * data){
	printf("\n\nThread id = %lu,\t Created in %ld iteration inside main\n", (unsigned long)pthread_self(), (long)data);
	pthread_exit(NULL);
}

int main () {
	int i;
	int n;
	printf("Enter the no. of threads you want to create\n");
	scanf("%d", &n);
	for(i = 0; i < n; i++) {
		int rc = pthread_create(&thread_id[i], NULL, PrintHello, (void *)(intptr_t)i);
		if (rc) {
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
		printf("\n Thread id = %lu,\t New thread %lu created in %d iteration\n\n", (unsigned long)pthread_self(), (unsigned long)thread_id[i], i);
	}
	pthread_exit(NULL);
}

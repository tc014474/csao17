#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct thread_data {
	int a;
	int b;
} thread_data;

void *myThread(void *arg)
{
	thread_data *tdata=(thread_data *)arg;
	int *result = malloc(sizeof(int));
	*result = tdata->a + tdata->b;
	pthread_exit((void *)result);
}

int main()
{
	pthread_t tid;
	thread_data tdata;
	void *result;
	printf("Enter the two valuse you want to add\n");
	scanf("%d%d", &tdata.a, &tdata.b);
	pthread_create(&tid, NULL, myThread, (void *)&tdata);
	pthread_join(tid, &result);
	printf("%d + %d = %d\n", tdata.a, tdata.b, *((int *)result));
	free(result);
	return 0;
}

#include <stdio.h>
#include <unistd.h>

void main()
{
   int ret = fork();
   if (ret == 0)
   {  /* this is the child process */
      printf("The child process ID is %d\n", getpid()); 
      printf("The childs parent process ID is %d\n", getppid());
   } else { /* this is the parent process */ 
      printf("The parent process ID is %d\n", getpid());
      printf("The parents parent process ID is %d\n", getppid());
   }
   sleep(2);
}


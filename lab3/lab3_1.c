#include <stdio.h>
#include <unistd.h>

void main()
{
    printf("Process ID is: %d\n", getpid());
    printf("Parent process ID is: %d\n", getppid());
}


#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int counter=0;

int main() {
	pid_t pid = fork();
	int counter=0;

	if (pid==-1) {
		perror("Error in fork");
		return 1;
	} else if (pid==0) {
		//child process
		for (int i=0; i<20; i++) {
			counter++;
			printf("The child process counter is %d\n", counter);
		}
	} else 
{
int counter=29;
		//parent process
		for (int i=0; i<20; i++) {
			counter++;
			printf("The parent process counter is %d\n", counter);
		}
	wait(NULL);
	}
return 0;
}
